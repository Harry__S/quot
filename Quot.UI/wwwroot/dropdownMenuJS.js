// JavaScript source code

/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("ddOptions").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
/*window.onclick = function (event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
} */ //Do not know if we need this, but keep it just in case. 

function GetSelectedValue() {
    var e = document.getElementById("ddOptions");
    var result = e.options[e.selectedIndex].value;

    document.getElementById("result").innerHTML = result;
}

function GetSelectedText() {
    var e = document.getElementById("ddOptions");
    var result = e.options[e.selectedIndex].text;

    document.getElementById("result").innerHTML = result;
}
