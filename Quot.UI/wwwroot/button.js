const button = document.getElementById("post-btn");

button.addEventListener("click", async x => {
  try {
    const dropdown = document.getElementById("reference");
    const url = "http://localhost:8080/v1/" + dropdown.value + "Search/" + document.getElementById("input").value;
    const response = await fetch(url)
      .then((response) => response.json())
      .then((data) => {
          if(data.length==0) {
              var popup = document.getElementById("errPopup");
              popup.classList.toggle("show");
              return;
          }

        const result = data[0];
        inText = data["inText"];
        inTextPrecise = data["inTextPrecise"];
        bibliography = data["bibliography"];
        document.getElementById("inText").innerHTML = "In Text Citation: " + data["inText"];
        document.getElementById("inTextPrecise").innerHTML = "Precise In Text Citation: " + data["inTextPrecise"];
        document.getElementById("bibliography").innerHTML = "Bibliography Citation: " + data["bibliography"];

      });
  } catch (err) {
    console.error(`Error: ${err}`);
  } 
});

$(x => {
  $(".response").dblclick(function(e){
    if($(this).attr("id") == "inText"){
      var temp = document.createElement("textarea");
      document.body.appendChild(temp);
      temp.value = document.getElementById("inText").innerText;
      temp.select()
      document.execCommand("copy");
      alert("Copied to clipboard");
      document.body.removeChild(temp);
    }
    else if($(this).attr("id") == "inTextPrecise"){
      var temp = document.createElement("textarea");
      document.body.appendChild(temp);
      temp.value = document.getElementById("inTextPrecise").innerText;
      temp.select()
      document.execCommand("copy");
      alert("Copied to clipboard");
      document.body.removeChild(temp);
    }
    else if($(this).attr("id") == "bibliography"){
      var temp = document.createElement("textarea");
      document.body.appendChild(temp);
      temp.value = document.getElementById("bibliography").innerText;
      temp.select()
      document.execCommand("copy");
      alert("Copied to clipboard");
      document.body.removeChild(temp);
    }    
  });
});

