﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Quot.Engine.Entities;
using Microsoft.AspNetCore.Cors;
using Quot.Engine.Services;
using Quot.Engine.Dto;
using AutoMapper;
using System.Collections.Generic;
using Quot.Engine.ReferenceBuilders;

namespace Quot.Engine.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class HarvardSearchController : ControllerBase
    {
        IParagraphService Service { get; set; }
        IHarvardRefBuilder Builder { get; set; }
        IMapper Mapper { get; set; }
        public HarvardSearchController(IParagraphService _service, IMapper _mapper, IHarvardRefBuilder _builder)
        {
            Service = _service;
            Mapper = _mapper;
            Builder = _builder;
        }

        [EnableCors]
        [HttpGet("{id}")]        
        public HarvardResponse Get(string id)
        {
            //Get works containing string
            IQueryable<Paragraph> paragraphs = Service.Get(id);
            //Convert works to dto (must convert to Array as AutoMapper does not support IQueryable)
            HarvardResponseDto[] dto = Mapper.Map<Paragraph[], HarvardResponseDto[]>(paragraphs.ToArray());
            //Convert back to Queryable for return type
            var response = Builder.Build(dto);
            return response;
        }
    }
}
