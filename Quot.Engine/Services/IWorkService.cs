﻿using Quot.Engine.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.Services
{
    public interface IWorkService
    {
        public IQueryable<Work> Get(String text);
    }
}
