﻿using Microsoft.EntityFrameworkCore;
using Quot.Engine.Db;
using Quot.Engine.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.Services
{
    public class SearchService : BaseService, ISearchService
    {
        public IWorkService workService;

        public SearchService(IDbContext _context, IWorkService _workService) : base(_context)
        {
            workService = _workService;
        }
        public IQueryable<Work> Get(string text)
        {
            //Select Paragraph objects from database which contain the search string supplied from the front-end. Also select paragraphs that are a substring of the supplied string from front-end, so searches can span multiple paragraphs.
            //TODO: Improve performance as DB is combed twice. Investigate other DB frameworks / other DB providers if limited by SQL functionality.
            var paragraphs = Context.Paragraphs.Where(o => o.PlainText.Contains(text) || text.Contains(o.PlainText.Replace("\n", ""))); //Use Replace("\n", "") because PlainText columns contain line feeds at the end of each string.
            //Create new queryable containing the workIds of each paragraph (including if there is more than one).
            IQueryable<string> workIds = paragraphs.Select(o => o.WorkId);
            //Create new list of works to store Work objects in.
            List<Work> works = new List<Work>();
            //Iterate through workIds and call WorkService to retrieve Work objects for those Ids and add them to the Works list.
            foreach(string w in workIds){
                works.AddRange(workService.Get(w));
            }

            return works.AsQueryable();
        }
    }
}