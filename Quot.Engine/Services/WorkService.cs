﻿using Quot.Engine.Db;
using Quot.Engine.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.Services
{
    public class WorkService : BaseService, IWorkService
    {
        public WorkService(IDbContext _context) : base(_context)
        {
        }
        public IQueryable<Work> Get(string text)
        {
            return Context.Works.Where(o => o.WorkId.Contains(text));
        }
    }
}
