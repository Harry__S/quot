﻿using Quot.Engine.Db;
using Quot.Engine.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quot.Engine.Services
{
    public class ParagraphService : BaseService, IParagraphService
    {
        public ParagraphService(IDbContext _context) : base(_context)
        {
        }
        public IQueryable<Paragraph> Get(string text)
        {
            var paras = Context.Paragraphs.AsEnumerable().Select(p => new Paragraph
            {
                WorkId = p.WorkId,
                ParagraphId = p.ParagraphId,
                ParagraphNum = p.ParagraphNum,
                PlainText = Regex.Replace(p.PlainText.Replace("[p]", ""), "[^a-zA-Z0-9]", ""),
                Section = p.Section,
                Chapter = p.Chapter
            });
            var rawText = Regex.Replace(text, "[^a-zA-Z0-9]", "");
            var results = paras.AsQueryable().Where(p => p.PlainText.ToLower().Contains(rawText.ToLower()));
            if (results.Count() > 0)
            {
                return results;
            }
            else
            {
                results = paras.AsQueryable().Where(p => rawText.ToLower().Contains(p.PlainText.ToLower()) && p.PlainText.Length > 10);
                return results;
            }
        }
    }
}
