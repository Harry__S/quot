﻿using Microsoft.Extensions.DependencyInjection;
using Quot.Engine.Db;
using Quot.Engine.Entities;
using Quot.Engine.ReferenceBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.Services
{
    public static class DependencyInjection
    {
        public static void ConfigureDI(this IServiceCollection services)
        {
            services.AddTransient<IDbContext, ShakespeareWorksContext>();

            services.AddTransient<ISearchService, SearchService>();

            services.AddTransient<IWorkService, WorkService>();

            services.AddTransient<IAuthorService, AuthorService>();

            services.AddTransient<IParagraphService, ParagraphService>();

            services.AddTransient<IAPARefBuilder, APARefBuilder>();

            services.AddTransient<IHarvardRefBuilder, HarvardRefBuilder>();

        }
    }
}
