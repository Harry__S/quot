﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Azure;
using Azure.Identity;
using Azure.Security.KeyVault.Secrets;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quot.Engine.Entities;

namespace Quot.Engine.Services
{
    public static class DbConfiguration
    {
        public static void AddDatabase(this IServiceCollection services, IConfiguration config)
        {
            var client = new SecretClient(vaultUri: new Uri("https://quotenginevault.vault.azure.net"), credential: new DefaultAzureCredential());

            try
            {
                KeyVaultSecret secret = client.GetSecret("Quot-Shakespeare");
                services.AddDbContext<ShakespeareWorksContext>(options =>
                {
                    var connString = secret.Value;
                    options.UseSqlServer(connString);
                });
            }
            catch (RequestFailedException ex)
            {
                Console.WriteLine(ex.ToString());
            }           
            
        }
    }
}
