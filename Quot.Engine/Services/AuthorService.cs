﻿using Quot.Engine.Db;
using Quot.Engine.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.Services
{
    public class AuthorService : BaseService, IAuthorService
    {
        public AuthorService(IDbContext _context) : base(_context)
        {
        }
        public IQueryable<Author> Get(string text)
        {
            return Context.Authors.Where(o => o.AuthorId.Equals(text));
        }
    }
}
