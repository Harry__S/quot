﻿using AutoMapper;
using Quot.Engine.Entities;
using Quot.Engine.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.Dto
{
    //Converts AuthorId to Author while mapping to a DTO using the AuthorService.
    public class PublishDateResolver : IValueResolver<Paragraph, ResponseDto, int>
    {
        public int Resolve(Paragraph source, ResponseDto destination, int destMember, ResolutionContext context)
        {
            return 1998;
        }
    }
}
