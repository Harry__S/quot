﻿using AutoMapper;
using Quot.Engine.Entities;
using Quot.Engine.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.Dto
{
    public class PublisherResolver : IValueResolver<Paragraph, ResponseDto, string>
    {
        IWorkService WorkService;

        public PublisherResolver(IWorkService _workService)
        {
            WorkService = _workService;
        }
        public string Resolve(Paragraph source, ResponseDto destination, string destMember, ResolutionContext context)
        {
            return WorkService.Get(source.WorkId).First().Source;
        }
    }
}
