﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.Dto
{
    public class APAResponseDto : ResponseDto
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public short? Date { get; set; }
        public int PublishDate { get; set; }
        public byte? Act { get; set; }
        public int? Scene { get; set; }
        public int? Line { get; set; }
        public string Publisher { get; set; }
        public string Edition { get; set; }

    }
}
