﻿using AutoMapper;
using Quot.Engine.Entities;
using Quot.Engine.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.Dto
{
    //Converts AuthorId to Author while mapping to a DTO using the AuthorService.
    public class DateResolver : IValueResolver<Paragraph, ResponseDto, short?>
    {
        IWorkService Service;
        public DateResolver(IWorkService _service)
        {
            Service = _service;
        }
        public short? Resolve(Paragraph source, ResponseDto destination, short? destMember, ResolutionContext context)
        {
            IQueryable<Work> work = Service.Get(source.WorkId);
            return work.FirstOrDefault().Date;
        }
    }
}