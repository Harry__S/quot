﻿using AutoMapper;
using Quot.Engine.Entities;
using Quot.Engine.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.Dto
{
    //Converts AuthorId to Author while mapping to a DTO using the AuthorService.
    public class AuthorResolver : IValueResolver<Paragraph, ResponseDto, string>
    {
        IAuthorService AuthorService;
        IWorkService WorkService;

        public AuthorResolver(IAuthorService _authorService, IWorkService _workService)
        {
            AuthorService = _authorService;
            WorkService = _workService;
        }
        public string Resolve(Paragraph source, ResponseDto destination, string destMember, ResolutionContext context)
        {
            return AuthorService.Get(WorkService.Get(source.WorkId).First().AuthorId).First().AuthorDisplayName;
        }
    }
}
