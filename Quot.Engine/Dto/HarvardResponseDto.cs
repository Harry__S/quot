﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.Dto
{
    public class HarvardResponseDto : ResponseDto
    {
        public string Author { get; set; }
        public int PublishDate { get; set; }
        public string Title { get; set; }
        public string Publisher { get; set; }
        public byte? Act { get; set; }
        public int? Scene { get; set; }
        public int? Line { get; set; }
        public string Edition { get; set; }
        public string Place { get; set; }

    }
}
