﻿using AutoMapper;
using Quot.Engine.Entities;
using Quot.Engine.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.Dto
{
    //Converts AuthorId to Author while mapping to a DTO using the AuthorService.
    public class TitleResolver : IValueResolver<Paragraph, ResponseDto, string>
    {
        IWorkService WorkService;

        public TitleResolver(IWorkService _workService)
        {
            WorkService = _workService;
        }
        public string Resolve(Paragraph source, ResponseDto destination, string destMember, ResolutionContext context)
        {
            return WorkService.Get(source.WorkId).First().Title;
        }
    }
}
