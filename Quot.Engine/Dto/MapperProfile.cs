﻿using AutoMapper;
using Quot.Engine.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.Dto
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Paragraph, APAResponseDto>()
                .ForMember(x => x.Title, opt => opt.MapFrom<TitleResolver>())
                .ForMember(x => x.Author, opt => opt.MapFrom<AuthorResolver>())
                .ForMember(x => x.Publisher, opt => opt.MapFrom<PublisherResolver>())
                .ForMember(x => x.Edition, opt => opt.MapFrom<EditionResolver>())
                .ForMember(x => x.PublishDate, opt => opt.MapFrom<PublishDateResolver>())
                .ForMember(x => x.Date, opt => opt.MapFrom<DateResolver>())
                .ForMember(x => x.Act, opt => opt.MapFrom(y => y.Section))
                .ForMember(x => x.Scene, opt => opt.MapFrom(y => y.Chapter))
                .ForMember(x => x.Line, opt => opt.MapFrom(y => y.ParagraphNum));

            CreateMap<Paragraph, HarvardResponseDto>()
                .ForMember(x => x.Title, opt => opt.MapFrom<TitleResolver>())
                .ForMember(x => x.Author, opt => opt.MapFrom<AuthorResolver>())
                .ForMember(x => x.Publisher, opt => opt.MapFrom<PublisherResolver>())
                .ForMember(x => x.PublishDate, opt => opt.MapFrom<PublishDateResolver>())
                .ForMember(x => x.Edition, opt => opt.MapFrom<EditionResolver>())
                .ForMember(x => x.Place, opt => opt.MapFrom<PlaceResolver>())
                .ForMember(x => x.Act, opt => opt.MapFrom(y => y.Section))
                .ForMember(x => x.Scene, opt => opt.MapFrom(y => y.Chapter))
                .ForMember(x => x.Line, opt => opt.MapFrom(y => y.ParagraphNum));
        }
    }
}
