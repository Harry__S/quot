﻿using Quot.Engine.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.ReferenceBuilders
{
    public class HarvardRefBuilder : IHarvardRefBuilder
    {
        public HarvardResponse Build(HarvardResponseDto[] responses)
        {
            HarvardResponseDto[] _harvardResponse = responses;
            HarvardResponse response = new HarvardResponse();
            response.InText = "(" + _harvardResponse.First().Author + ", " + _harvardResponse.First().PublishDate + ")";
            if (_harvardResponse.Length > 1)
            {
                response.InTextPrecise = "(" + _harvardResponse.First().Author + ", " + _harvardResponse.First().PublishDate + ", " + _harvardResponse.First().Act + "." + _harvardResponse.First().Scene + ":" + _harvardResponse.First().Line + "-" + _harvardResponse.Last().Line + ")";
                response.Bibliography = _harvardResponse.First().Author + ", W. (" + _harvardResponse.First().PublishDate + "). <em>" + _harvardResponse.First().Title + "</em>. Edited by E.M.Johnson. " + _harvardResponse.First().Place + ": " + _harvardResponse.First().Publisher + ". " + _harvardResponse.First().Act + "." + _harvardResponse.First().Scene + ":" + _harvardResponse.First().Line + "-" + _harvardResponse.Last().Line + ".";
            }
            else
            {
                response.InTextPrecise = "(" + _harvardResponse.First().Author + ", " + _harvardResponse.First().PublishDate + ", " + _harvardResponse.First().Act + "." + _harvardResponse.First().Scene + ":" + _harvardResponse.First().Line + ")";
                response.Bibliography = _harvardResponse.First().Author + ", W. (" + _harvardResponse.First().PublishDate + "). <em>" + _harvardResponse.First().Title + "</em>. Edited by E.M.Johnson. " + _harvardResponse.First().Place + ": " + _harvardResponse.First().Publisher + ". " + _harvardResponse.First().Act + "." + _harvardResponse.First().Scene + ":" + _harvardResponse.First().Line + ".";
            }
            
            return response;
        } 
    }
}
