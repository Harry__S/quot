﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.ReferenceBuilders
{
    public class HarvardResponse
    {
        public string InText { get; set; }
        public string InTextPrecise { get; set; }
        public string Bibliography { get; set; }
    }
}
