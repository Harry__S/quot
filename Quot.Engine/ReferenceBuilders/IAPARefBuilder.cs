﻿using Quot.Engine.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.ReferenceBuilders
{
    public interface IAPARefBuilder
    {
        public APAResponse Build(APAResponseDto[] responses);
    }
}
