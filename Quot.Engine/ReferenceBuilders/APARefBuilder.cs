﻿using Quot.Engine.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quot.Engine.ReferenceBuilders
{
    public class APARefBuilder : IAPARefBuilder
    {
        public APAResponse Build(APAResponseDto[] responses)
        {
            APAResponseDto[] _apaResponse = responses;
            APAResponse response = new APAResponse();
            response.InText = "(" + _apaResponse.First().Author + ", " + _apaResponse.First().Date + ")";
            if (_apaResponse.Length > 1)
            {
                response.InTextPrecise = "(" + _apaResponse.First().Author + ", " + _apaResponse.First().Date + ", " + _apaResponse.First().Act + "." + _apaResponse.First().Scene + "." + _apaResponse.First().Line + "-" + _apaResponse.Last().Line + ")";
            }
            else
            {
                response.InTextPrecise = "(" + _apaResponse.First().Author + ", " + _apaResponse.First().Date + ", " + _apaResponse.First().Act + "." + _apaResponse.First().Scene + "." + _apaResponse.First().Line + ")";
            }
            response.Bibliography = _apaResponse.First().Author + ", W. (" + _apaResponse.First().PublishDate + "). <em>" + _apaResponse.First().Title + "</em>. E.M.Johnson. " + _apaResponse.First().Publisher + ". (" + _apaResponse.First().Date + ").";
            return response;
        } 
    }
}
